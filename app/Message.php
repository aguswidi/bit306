<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['message', 'sender', 'recipient'];
    
    public $timestamps = false;

    public function fr(){
        return $this->belongsTo('App\User', 'sender');
    }
    
}
