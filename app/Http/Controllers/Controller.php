<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use File;
use Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected function upload($image){
        $filename  = time() .'.'.$image->getClientOriginalExtension();
        $path = public_path('image/'.$filename);
        Image::make($image->getRealPath())->fit(200)->save($path);
        return url('image/'.$filename);
    }

    protected function delete($avatar){
        $url =  config('app.url').'/'; 
        $path = str_replace($url, '', $avatar);
        
        if(File::exists($path)){
            File::delete($path);
            return true;
        }else{
            return false;
        }
    }
}
