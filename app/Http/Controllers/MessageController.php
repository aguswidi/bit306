<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Message;

class MessageController extends Controller
{
    public function index(){
        $id = Auth::id();
        $messages = DB::select('SELECT * FROM messages LEFT JOIN users ON messages.sender = users.id WHERE recipient = ?', [$id]);
        return view('message', compact('messages'));
    }

    private function rules()
    {
        return [
            'message' => ['required', 'string', 'max:255'],
            'user_id' => ['required', 'integer']
        ];
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect('user/'.Auth::id())
                        ->withErrors($validator)
                        ->withInput();
        }

        $message = Message::create([
            'message'=>$request->message,
            'recipient'=>$request->user_id,
            'sender'=>Auth::id()
        ]);
        return redirect('home');
    }

    public function delete($id){
        DB::delete('DELETE FROM messages WHERE id = ?', [$id]);
        return redirect('message');
    }
}
