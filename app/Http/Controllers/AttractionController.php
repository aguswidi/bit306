<?php

namespace App\Http\Controllers;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Attraction;

class AttractionController extends Controller
{
    private function rules()
    {
        return [
            'title' => ['required', 'string', 'max:255'],
            'picture' => ['required', 'image', 'max:10000', 'mimes:jpeg,png,jpg'],
            'about' => ['required', 'string', 'max:8000'],
        ];
    }

    public function index(){
        $attractions = Attraction::paginate(10);
        return view('attraction', compact('attractions'));
    }

    public function newForm(){
        return view('attraction-new');
    }

    public function create(Request $request){
        $validator = Validator::make($request->all(), $this->rules());
        if ($validator->fails()) {
            return redirect('attraction/form')
                        ->withErrors($validator)
                        ->withInput();
        }

        $attraction = [
            Auth::id(),
            $request->title,
            parent::upload($request->picture),
            $request->about,
        ];
        DB::insert('INSERT INTO attractions (user_id, title, picture, description) VALUES (?, ?,?,?)', $attraction);
        return redirect('attraction');
    }

    public function view($id){
        $attraction = DB::select('SELECT * FROM attractions WHERE id = ?', [$id]);
        if(sizeOf($attraction)>0){
            $attraction = $attraction[0];
            return view('attraction-detail', compact('attraction'));
        }else{
            $attraction = null;
        }
        
    }

    public function delete($id){
        DB::delete('DELETE FROM attractions WHERE id = ?', [$id]);
        return redirect('attraction');
    }
}
