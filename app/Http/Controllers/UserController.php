<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function updateForm(){
        $user = Auth::user();
        return view('update', compact('user'));
    }

    private function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'interest' => ['required', 'array'],
            'photo' => ['image', 'max:10000', 'mimes:jpeg,png,jpg'],
            'age_group' => ['required', 'string', 'max:255'],
            'home_town' => ['required', 'string', 'max:255'],
            'about' => ['required', 'string', 'max:8000'],
        ];
    }

    
    public function update(Request $data)
    {   
        $validator = Validator::make($data->all(), $this->rules());
        if ($validator->fails()) {
            return redirect('user/update')
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = Auth::user();
        if(isset($data->photo)){
            parent::delete($user->photo);
            $photo = parent::upload($data->photo);
        }else{
            $photo = $user->photo;
        }
        
        DB::update('UPDATE users SET name=?, interest=?, photo=?, age_group=?, home_town=?, about=? WHERE id=?', [
            $data->name,
            json_encode($data->interest),
            $photo,
            $data->age_group,
            $data->home_town,
            $data->about,
            $user->id]);

        return redirect('home');
    }

    public function view($id){
        $user = DB::select('SELECT * FROM users WHERE id = ?', [$id]);
        $user = $user[0];
        return view('user-detail', compact('user'));
    }


}
