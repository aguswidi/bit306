<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    public function index(){
        return view('auth.register');
    } 

    private function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
            'type' => ['required', 'integer'],
            'interest' => ['required', 'array'],
            'photo' => ['required', 'image', 'max:10000', 'mimes:jpeg,png,jpg'],
            'age_group' => ['required', 'string', 'max:255'],
            'home_town' => ['required', 'string', 'max:255'],
            'about' => ['required', 'string', 'max:8000'],
        ];
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $data)
    {   
        $validator = Validator::make($data->all(), $this->rules());
        if ($validator->fails()) {
            return redirect('register')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = [
            $data->name,
            $data->email,
            Hash::make($data->password),
            $data->type,
            json_encode($data->interest),
            $data->about,
            parent::upload($data->photo),
            $data->age_group,
            $data->home_town
        ];
        DB::insert('INSERT INTO users (name, email, password, type, interest, about, photo, age_group, home_town) VALUES (?,?,?,?,?,?,?,?,?)', $user);

        return redirect('login');
    }
}
