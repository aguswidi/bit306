<?php



Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::group(['middleware' => 'guest'], function(){
    Route::post('register', 'Auth\RegisterController@register');
    Route::get('register', 'Auth\RegisterController@index')->name('register');


    Route::get('login', 'Auth\RegisterController@index');

    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    
});

Route::get('/home', 'AttractionController@index')->name('home');

Route::group(['middleware' => 'auth:web'], function(){
    Route::get('attraction', 'AttractionController@index')->middleware('can:guide')->name('attraction.all');
    Route::get('attraction/form', 'AttractionController@newForm')->middleware('can:guide')->name('attraction.form');
    Route::post('attraction', 'AttractionController@create')->middleware('can:guide')->name('attraction.new');
    Route::get('attraction/delete/{id}', 'AttractionController@delete')->middleware('can:guide')->name('attraction.delete');
    

    
    Route::get('message', 'MessageController@index')->middleware('can:guide')->name('message.all');
    Route::post('message/create', 'MessageController@create')->middleware('can:visitor');
    Route::get('message/delete/{id}', 'MessageController@delete')->middleware('can:guide');

    Route::get('user/update', 'UserController@updateForm')->name('user.update');
    Route::post('user/update', 'UserController@update');
    Route::get('user/{id}', 'UserController@view');
});

Route::get('/', 'AttractionController@index');
Route::get('attraction/{id}', 'AttractionController@view');