@extends('layouts.app')

@section('title')
    Attraction Detail
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-lg-6 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="pull-left">{{ $attraction->title }}</h4>
                    @guest
                    @else
                    @if (Auth::user()->type == 2)
                        <a href="{{ url('attraction/delete/'.$attraction->id) }}" type="button" class="pull-right btn btn-sm btn-rounded btn-outline-danger">
                            <i class="fa fa-trash"></i> Delete
                        </a>
                    @elseif (Auth::user()->type == 1)
                        <a href="{{ url('user/'.$attraction->user_id) }}" type="button" class="pull-right btn btn-sm btn-rounded btn-outline-primary">
                            <i class="fa fa-user"></i> View Guide
                        </a>
                    @endif
                    @endguest
                </div>
                <div class="card-body">
                <img class="card-img-top img-responsive" src="{{ $attraction->picture }}" alt="Card image cap">
                    <p>{{ $attraction->description }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
