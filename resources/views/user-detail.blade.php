
<?php
$interests = json_decode($user->interest);
?>
@extends('layouts.app')

@section('title')
    User Detail
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        
        <div class="col-md-6 col-lg-6 col-sm-12">
            <div class="card">
                <div class="card-body">
                <img class="card-img-top img-responsive" src="http://localhost:8000/vendor/img/default.jpg" alt="Card image cap">
                    <h2 class="mt-3">{{ $user->name }}</h2>
                    <hr>
                    <p>{{ $user->email }}</p>
                    <hr>
                    <p>{{ $user->home_town }}</p>
                    <hr>
                    <p>{{ $user->age_group }}</p>
                    <hr>
                    <p>{{ $user->about }}</p>
                    <hr>
                    @foreach($interests as $interest)
                        {{ $interest.', ' }}
                    @endforeach
                    
                </div>
            </div>

            <div class="card mt-2">
                <div class="card-header">{{ __('Message') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('message/create') }}">
                        @csrf

                        <div class="form-group row">
                            <textarea id="message" rows="4" type="text" class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}" name="message" value="{{ old('message') }}">{{ old('message') }}</textarea>
                            @if ($errors->has('message'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('message') }}</strong>
                                </span>
                            @endif
                        </div>

                        <input type="hidden" name=user_id value="{{ $user->id }}">

                        <div class="form-group row mb-0">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Send') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection
