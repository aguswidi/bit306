@extends('layouts.app')

@section('title')
    Message
@endsection

@section('content')
<div class="container">
    <div class="row">
        @foreach($messages as $message)
        <div class="col-md-12">
            <div class="card mb-4">
                <div class="card-header">
                    <h4 class="pull-left">{{ $message->name }}</h4>
                    <a href="{{ url('message/delete/'.$message->id) }}">
                        <button type="button" class="pull-right btn btn-sm btn-rounded btn-outline-danger">
                            <i class="fa fa-trash"></i> Delete
                        </button>
                    </a>
                </div>
                <p class="card-header">{{ $message->message }}</p>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
