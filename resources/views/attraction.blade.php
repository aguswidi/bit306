@extends('layouts.app')

@section('title')
    Attraction
@endsection

@section('content')
<div class="container">
    <div class="row">
        @foreach($attractions as $attraction)
        <div class="col-md-3"> <img class="card-img-top img-responsive" src="{{ $attraction->picture }}" alt="Card image cap">
            <div class="card mb-4">
                <a href="{{ url('attraction/'.$attraction->id) }}"><h4 class="card-header">{{ $attraction->title }}</h4>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
