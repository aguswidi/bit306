<?php
    $interests = json_decode($user->interest);
    
?>

@extends('layouts.app')

@section('title')
    Update Profile
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Update') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('user/update') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="e.g. John Cena" value="{{ $user->name }}" autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('Age') }}</label>

                            <div class="col-md-6">
                                <select id="age"  class="form-control{{ $errors->has('age_group') ? ' is-invalid' : '' }}" name="age_group">
                                    <option value="18-25" {{ $user->age_group == "18-25" ? "selected" : "" }}>18-25</option>
                                    <option value="26-35" {{ $user->age_group == "26-35" ? "selected" : "" }}>26-35</option>
                                    <option value="36-45" {{ $user->age_group == "36-45" ? "selected" : "" }}>36-45</option>
                                    <option value="above 45" {{ $user->age_group == "above 45" ? "selected" : "" }}>above 45</option>
                                </select>
                                @if ($errors->has('age_group'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('age_group') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="home-town" class="col-md-4 col-form-label text-md-right">{{ __('Home Town') }}</label>

                            <div class="col-md-6">
                                <input id="home-town" type="text" placeholder="e.g. Denpasar" class="form-control{{ $errors->has('home_town') ? ' is-invalid' : '' }}" name="home_town" value="{{ $user->home_town }}">

                                @if ($errors->has('home_town'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('home_town') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="photo" class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}</label>

                            <div class="col-md-6">
                                <input id="photo" type="file" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}" name="photo">

                                @if ($errors->has('photo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('photo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="interest" class="col-md-4 col-form-label text-md-right">{{ __('Interest') }}</label>

                            <div class="col-md-6">
                            <select multiple id="interest"  class="form-control{{ $errors->has('interest') ? ' is-invalid' : '' }}" name="interest[]">
                                    <option value="music" {{ in_array("music", $interests) ? "selected" : "" }}>music</option>
                                    <option value="art" {{ in_array("art", $interests) ? "selected" : "" }}>art</option>
                                    <option value="dance" {{ in_array("dance", $interests) ? "selected" : "" }}>dance</option>
                                    <option value="food" {{ in_array("food", $interests) ? "selected" : "" }}>food</option>
                                    <option value="culture" {{ in_array("culture", $interests) ? "selected" : "" }}>culture</option>
                                </select>

                                @if ($errors->has('interest'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('interest') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="about" class="col-md-4 col-form-label text-md-right">{{ __('About Me') }}</label>

                            <div class="col-md-6">
                                <textarea id="about" rows="4" type="text" class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}" name="about">{{ $user->about }}</textarea>
                                @if ($errors->has('about'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
